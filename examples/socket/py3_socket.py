#!/usr/bin/env python3
import socket

try:
    import cPickle as pickle
except ImportError:
    import pickle

# create a socket object
s = socket.socket()
print("Socket successfully created")

# localhost
host_ip = '127.0.0.1'

# default port for socket
port = 17292

# connect to the server on local computer
s.connect((host_ip, port))
# receive data from the server
#print(pickle.loads(s.recv(1024)))
while True:
    if int(input()):
        s.send(bytes('random', 'utf-8'))
    else:
        s.send(bytes('nan', 'utf-8'))
    print(s.recv(1024))
# close the connection
    #s.close()