#!/usr/bin/env python

"""
- Open socket
    localhost:1729

- TF listener
    get vector and quaternion for all links wrt base

- Payload:
    dict {link_name:[vector, quaternion]}

- 

"""

import socket
import random

try:
    import cPickle as pickle
except ImportError:
    import pickle

# create a socket object
s = socket.socket()         
print "Socket successfully created"
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
# port number
port = 17292

payload = {'link1':[[1,2,3,4],[1,2,1]],
           'link2':[[1,2,3,4],[1,2,2]],
           'link3':[[1,2,3,4],[1,2,3]]}

pickled_payload = pickle.dumps(payload, -1)

s.bind(('', port))        
print "socket binded to %s" %(port)

# put the socket into listening mode
s.listen(5)     
print "socket is listening"            
c, addr = s.accept() 
print 'Got connection from', addr
# a forever loop until we interrupt it or 
# an error occurs
while True:
    # Establish connection with client.
       
    

    r = c.recv(1024)
    print r

    # send a thank you message to the client. 
    if r == 'random': c.send(str(random.random()))
    else: c.send(str(random.random()))
    # Close the connection with the client
    #c.close()  
