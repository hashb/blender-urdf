#!/usr/bin/env python
import numpy as np
from urdf_parser_py.urdf import Robot
from pykdl_utils.kdl_parser import kdl_tree_from_urdf_model
from pykdl_utils.kdl_kinematics import KDLKinematics

def quaternion_from_matrix(matrix, isprecise=False):
    """Return quaternion from rotation matrix.
    Quaternions w+ix+jy+kz are represented as [w, x, y, z].

    If isprecise is True, the input matrix is assumed to be a precise rotation
    matrix and a faster algorithm is used.

    """
    M = numpy.array(matrix, dtype=numpy.float64, copy=False)[:4, :4]
    if isprecise:
        q = numpy.empty((4, ))
        t = numpy.trace(M)
        if t > M[3, 3]:
            q[0] = t
            q[3] = M[1, 0] - M[0, 1]
            q[2] = M[0, 2] - M[2, 0]
            q[1] = M[2, 1] - M[1, 2]
        else:
            i, j, k = 1, 2, 3
            if M[1, 1] > M[0, 0]:
                i, j, k = 2, 3, 1
            if M[2, 2] > M[i, i]:
                i, j, k = 3, 1, 2
            t = M[i, i] - (M[j, j] + M[k, k]) + M[3, 3]
            q[i] = t
            q[j] = M[i, j] + M[j, i]
            q[k] = M[k, i] + M[i, k]
            q[3] = M[k, j] - M[j, k]
        q *= 0.5 / math.sqrt(t * M[3, 3])
    else:
        m00 = M[0, 0]
        m01 = M[0, 1]
        m02 = M[0, 2]
        m10 = M[1, 0]
        m11 = M[1, 1]
        m12 = M[1, 2]
        m20 = M[2, 0]
        m21 = M[2, 1]
        m22 = M[2, 2]
        # symmetric matrix K
        K = numpy.array([[m00-m11-m22, 0.0,         0.0,         0.0],
                         [m01+m10,     m11-m00-m22, 0.0,         0.0],
                         [m02+m20,     m12+m21,     m22-m00-m11, 0.0],
                         [m21-m12,     m02-m20,     m10-m01,     m00+m11+m22]])
        K /= 3.0
        # quaternion is eigenvector of K that corresponds to largest eigenvalue
        w, V = numpy.linalg.eigh(K)
        q = V[[3, 0, 1, 2], numpy.argmax(w)]
    if q[0] < 0.0:
        numpy.negative(q, q)
    return q

class urdf_kinematics(object):
    """docstring for urdf_kinematics"""
    def __init__(self, file_name, ):
        """
        do some stuff
        """
        
        # Read the file
        urdf_file = file(file_name, 'r')
        self.robot = Robot.from_xml_string(urdf_file.read())
        urdf_file.close()

        # hardcoded for now
        # TODO change these
        self.init_pos = np.array([[1,0,0,0],
                                 [0,1,0,0.7],
                                 [0,0,1,0.6],
                                 [0,0,0,1]])
        self.init_rot = np.array([[1,0,0,0],
                                 [0,0,1,0],
                                 [0,-1,0,0],
                                 [0,0,0,1]])
        
        # initialize some useful variables
        self.child_map = self.robot.child_map
        self.parent_map = self.robot.child_map
        self.links = dfs(self.child_map, (None, self.robot.get_root()))
        self.joints = dfs(self.child_map, (None, self.robot.get_root()), j=True)
        self.joint_limits = {joint.name:(joint.limit.lower, joint.limit.upper) if joint.limit is not None else None for joint in robot.joints}

        self.mesh_file_names = [{link.name:link.visual.geometry.filename if link.visual is not None else None} for link in robot.links]

        self.init_joint_angles, self.init_joint_angles_dict = random_init_joint_angles()

        # TODO find a way to correct root link problem
        self.root = 'base_link'

        self.vectors, self.quaternions = fk()

    def dfs(self, graph, start, j=False):
        visited, stack = [], [start]
        joints = []
        while stack:
            vertex = stack.pop()
            if vertex[1] not in set(visited):
                visited.append(vertex[1])
                joints.append(vertex[0])
                try:
                    stack.extend(set(graph[vertex[1]]) - set(visited))
                except Exception as e:
                    pass
        if j:
            return joints[1:]
        return visited

    def fk(self):
        vectors = []
        quaternions = []

        for link in self.links[1:]:
            kdl_kin = KDLKinematics(self.robot, self.root, link)
            pose = kdl_kin.forward(self.init_joint_angles_dict[self.child_map[link][0]])
            n_pose = init_pos*pose
            n_pose = n_pose*init_rot
            vectors.append(n_pose[0:3, -1].T.tolist())
        return vectors, quaternions

    def random_init_joint_angles(self):
        joint_angles = []
        joint_angles_dict = {}

        for joint in self.joints:
            if self.joint_limits[joint] is not None:
                lims = self.joint_limits[joint]
                q = np.random.uniform(lims[0], lims[1])
                joint_angles.append(q)
                joint_angles_dict[joint] = q
        return joint_angles, joint_angles_dict
