#!/usr/bin/env python

# using pykdl_utils and urdf_parser_py 
# parse a urdf file and write the tree structure to a file
# 

from urdf_parser_py.urdf import Robot
from pykdl_utils.kdl_parser import kdl_tree_from_urdf_model
from pykdl_utils.kdl_kinematics import KDLKinematics
import numpy as np
import numpy

def quaternion_from_matrix(matrix, isprecise=False):
    """Return quaternion from rotation matrix.
    Quaternions w+ix+jy+kz are represented as [w, x, y, z].

    If isprecise is True, the input matrix is assumed to be a precise rotation
    matrix and a faster algorithm is used.

    """
    M = numpy.array(matrix, dtype=numpy.float64, copy=False)[:4, :4]
    if isprecise:
        q = numpy.empty((4, ))
        t = numpy.trace(M)
        if t > M[3, 3]:
            q[0] = t
            q[3] = M[1, 0] - M[0, 1]
            q[2] = M[0, 2] - M[2, 0]
            q[1] = M[2, 1] - M[1, 2]
        else:
            i, j, k = 1, 2, 3
            if M[1, 1] > M[0, 0]:
                i, j, k = 2, 3, 1
            if M[2, 2] > M[i, i]:
                i, j, k = 3, 1, 2
            t = M[i, i] - (M[j, j] + M[k, k]) + M[3, 3]
            q[i] = t
            q[j] = M[i, j] + M[j, i]
            q[k] = M[k, i] + M[i, k]
            q[3] = M[k, j] - M[j, k]
        q *= 0.5 / math.sqrt(t * M[3, 3])
    else:
        m00 = M[0, 0]
        m01 = M[0, 1]
        m02 = M[0, 2]
        m10 = M[1, 0]
        m11 = M[1, 1]
        m12 = M[1, 2]
        m20 = M[2, 0]
        m21 = M[2, 1]
        m22 = M[2, 2]
        # symmetric matrix K
        K = numpy.array([[m00-m11-m22, 0.0,         0.0,         0.0],
                         [m01+m10,     m11-m00-m22, 0.0,         0.0],
                         [m02+m20,     m12+m21,     m22-m00-m11, 0.0],
                         [m21-m12,     m02-m20,     m10-m01,     m00+m11+m22]])
        K /= 3.0
        # quaternion is eigenvector of K that corresponds to largest eigenvalue
        w, V = numpy.linalg.eigh(K)
        q = V[[3, 0, 1, 2], numpy.argmax(w)]
    if q[0] < 0.0:
        numpy.negative(q, q)
    return q

def compute_fk():
    pass

def gen_rand_joint_angles():
    pass

def dfs(graph, start):
    visited, stack = [], [start]
    joints = []
    while stack:
        #print visited
        vertex = stack.pop()
        if vertex[1] not in set(visited):
            visited.append(vertex[1])
            joints.append(vertex[0])
            try:
                stack.extend(set(graph[vertex[1]]) - set(visited))
                #print 'worked'
            except Exception as e:
                print e, 'exception'
    return joints

urdf_file_name = "/home/hashb/catkin_ws/src/urlg_robots_description/robots/lbr4_allegro_right.urdf"

urdf_file = file(urdf_file_name, 'r')
robot = Robot.from_xml_string(urdf_file.read())
print "read the robot"
urdf_file.close()

# print robot.joint_map
print "visited"
print dfs(robot.child_map, (None,robot.get_root()))[1:]
# links = [[link.name, link.visual.geometry.filename if link.visual is not None else None] for link in robot.links]
# print links
# print {joint.name:(joint.limit.lower, joint.limit.upper) if joint.limit is not None else None for joint in robot.joints}

tree = kdl_tree_from_urdf_model(robot)
# print '\nNum segements:',tree.getNrOfSegments(),'\n'
# print robot.get_root()
kdl_kin = KDLKinematics(robot, 'base_link', 'middle_tip')
q = kdl_kin.random_joint_angles()

print 'length of q: ', len(q)
pose = kdl_kin.forward(q) # forward kinematics (returns homogeneous 4x4 numpy.mat)

print 'Joint Angles q:\n', q, '\n'

init_pos = np.array([[1,0,0,0],
                     [0,1,0,0.7],
                     [0,0,1,0.6],
                     [0,0,0,1]])
init_rot = np.array([[1,0,0,0],
                     [0,0,1,0],
                     [0,-1,0,0],
                     [0,0,0,1]])

n_pose = init_pos*pose
n_pose = n_pose*init_rot

n_pose_rot = np.eye(4)
n_pose_rot[0:3, 0:3] = n_pose[0:3, 0:3]

print "quaternion: ",
print quaternion_from_matrix(n_pose[0:3, 0:3])

pose_rot = np.eye(4)
pose_rot[0:3, 0:3] = pose[0:3, 0:3]

print "vector: ", n_pose[0:3, -1].T.tolist()
#print 'current pose:\n', tuple(map(tuple, np.asarray(n_pose)))


# base_link
# lbr4_0_link
# lbr4_1_link
# lbr4_2_link
# lbr4_3_link
# lbr4_4_link
# lbr4_5_link
# lbr4_6_link
# lbr4_7_link
# lbr4_mount
# allegro_mount
# palm_link
# middle_link_0
# middle_link_1
# middle_link_2
# middle_link_3
# middle_tip
