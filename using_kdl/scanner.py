#!BPY

import bpy
from bpy import data as D
from bpy import context as C
from mathutils import *
from math import *

import blensor

_BASE_FILE_PATH = '/home/hashb/Data/YCB_PCD/ROB1/'

"""If the scanner is the default camera it can be accessed 
  for example by bpy.data.objects["Camera"]"""
scanner_1 = bpy.data.objects["Camera_1"]
scanner_2 = bpy.data.objects["Camera_2"]
scanner_3 = bpy.data.objects["Camera_3"]

file_paths = open("/home/hashb/Dropbox/_GRAD_SCHOOL/Research/ycb/files.txt").read().split('\n')
dim_file = open(_BASE_FILE_PATH + 'dimensions.txt','w')

for file in file_paths:
    # Import object into the workspace
    bpy.ops.import_scene.obj(filepath=file)
    imported = C.selected_objects[0]
    print('\n\n' + imported.name + '\n\n')
    obj = bpy.data.objects[imported.name]
    obj.location = Vector((0.0, 0.0, 1.01))
    obj.rotation_euler = (0, 0, 0)
    dim_file.write(str(obj.dimensions))

    name = file.split('/')[-3] + '_ROBOT1'
    print('\n\nscanning file: ' + name + '\n')

    # Activate Camera_1
    C.scene.camera = scanner_1
    blensor.kinect.scan_advanced(scanner_1, evd_file=_BASE_FILE_PATH+name+'_CAMERA1.pcd')
    blensor.kinect.scan_advanced(scanner_1, evd_file=_BASE_FILE_PATH+name+'_CAMERA1.pgm')

    # Activate Camera_2
    C.scene.camera = scanner_2
    blensor.kinect.scan_advanced(scanner_2, evd_file=_BASE_FILE_PATH+name+'_CAMERA2.pcd')
    blensor.kinect.scan_advanced(scanner_2, evd_file=_BASE_FILE_PATH+name+'_CAMERA2.pgm')

    # Activate Camera_3
    C.scene.camera = scanner_3
    blensor.kinect.scan_advanced(scanner_3, evd_file=_BASE_FILE_PATH+name+'_CAMERA3.pcd')
    blensor.kinect.scan_advanced(scanner_3, evd_file=_BASE_FILE_PATH+name+'_CAMERA3.pgm')

    for ob in bpy.context.scene.objects:
        ob.select = ob.name.startswith("optimized_tsdf")
        bpy.ops.object.delete()
        ob.select = ob.name.startswith("Noisy")
        bpy.ops.object.delete()

file_paths.close()
dim_file.close()

print('Run Complete.')