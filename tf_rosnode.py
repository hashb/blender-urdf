#!/usr/bin/env python
"""

"""

import rospy
import tf

import numpy as np
from urdf_parser_py.urdf import Robot
from pykdl_utils.kdl_parser import kdl_tree_from_urdf_model
from pykdl_utils.kdl_kinematics import KDLKinematics

def urdf_parser(filename):
    """
    urdf parser
    
    returns a list of links and mesh files
    # TODO make it into a class and save useful params/variables
    """
    urdf_file = file(filename, 'r')
    robot = Robot.from_xml_string(urdf_file.read())
    urdf_file.close()

    return [{link.name:link.visual.geometry.filename} \
            if link.visual is not None for link in robot.links]


#######################
## listener (socket) ##
#######################
class comm_engine():
    """docstring for listener_socket"""
    def __init__(self, mode, port=1729):
        # init socket
        self.s = socket.socket()
        # decrease timeout 
        self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # IP address and port
        host_ip = '127.0.0.1'
        port = port

        if mode == 'bind':
            # Bind to port
            self.s.bind((host_ip, port))
        else:
            # Connect to port
            self.s.connect((host_ip, port))
        
    def listener(self):
        """
        recieves info from ROS Node
        """
        self.data = pickle.loads(self.s.recv(1024))
        return self.data

    def transmitter(self, message):
        """
        Transmits a message to the ROS Node
        """
        self.s.send(bytes(message, 'utf-8'))
        return self.listener()


def get_pose(listener, rate):
    if not rospy.is_shutdown():
        try:
            (trans,rot) = listener.lookupTransform('/turtle2', '/turtle1', rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue

        rate.sleep()

if __name__ == '__main__':
    rospy.init_node('blensor')

    listener = tf.TransformListener()

    rate = rospy.Rate(10.0)
