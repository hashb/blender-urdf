#!BPY
"""
Blender script written in python 3

"""

import bpy
from bpy import data as D
from bpy import context as C
from mathutils import *
from math import *

import blensor

import socket

try:
    import cPickle as pickle
except ImportError:
    import pickle


####################
## main variables ##
####################
# TODO update these variables
YCB_DIR = ''
SAVE_DIR = '/home/hashb/Data/YCB_PCD/'
POSE_NO = 0


#########################
## mesh files importer ##
#########################

def mesh_io(data):
    for x in data:
        bpy.ops.import_mesh.stl(filepath=x)


######################################
## Updates position and orientation ##
######################################
def update_pos(data):
    for name in data.keys():
        obj = bpy.data.objects[name]
        pose = data[name]
        obj.location = Vector(tuple(pose[0]))
        obj.rotation_quaternion = tuple(pose[1])


#######################
## listener (socket) ##
#######################
class comm_engine():
    """docstring for listener_socket"""
    def __init__(self, mode, port=1729):
        # init socket
        self.s = socket.socket()
        # decrease timeout 
        self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # IP address and port
        host_ip = '127.0.0.1'
        port = port

        if mode == 'bind':
            # Bind to port
            self.s.bind((host_ip, port))
        else:
            # Connect to port
            self.s.connect((host_ip, port))
        
    def listener(self):
        """
        recieves info from ROS Node
        """
        self.data = pickle.loads(self.s.recv(1024))
        return self.data

    def transmitter(self, message):
        """
        Transmits a message to the ROS Node
        """
        self.s.send(bytes(message, 'utf-8'))
        return self.listener()


#############
## Scanner ##
#############

# We define the camera names in the blend file.
scanner_1 = bpy.data.objects["Camera_1"]
scanner_2 = bpy.data.objects["Camera_2"]
scanner_3 = bpy.data.objects["Camera_3"]

# file names
file_paths = open("/home/hashb/Dropbox/_GRAD_SCHOOL/Research/ycb/files.txt").read().split('\n')
file_paths.close()

def scanner(): 
    """
    runs the scanner over all the files in the ycb dataset
    """
    for file in file_paths:
        # Import object into the workspace
        bpy.ops.import_scene.obj(filepath=file)
        
        # Select the object imported
        imported = C.selected_objects[0]
        print('\n\n' + imported.name + '\n\n')
        obj = bpy.data.objects[imported.name]

        # Pose
        obj.location = Vector((0.0, 0.0, 1.01))
        obj.rotation_euler = (0, 0, 0)

        name = file.split('/')[-3]
        print('\n\nscanning file: ' + name + '\n')

        directory = SAVE_DIR + '/' + 'pose_' + str(POSE_NO) + '/' + name

        # Activate Camera_1
        C.scene.camera = scanner_1
        blensor.kinect.scan_advanced(scanner_1, evd_file=directory+'_CAMERA1.pcd')
        blensor.kinect.scan_advanced(scanner_1, evd_file=directory+'_CAMERA1.pgm')

        # Activate Camera_2
        C.scene.camera = scanner_2
        blensor.kinect.scan_advanced(scanner_2, evd_file=directory+'_CAMERA2.pcd')
        blensor.kinect.scan_advanced(scanner_2, evd_file=directory+'_CAMERA2.pgm')

        # Activate Camera_3
        C.scene.camera = scanner_3
        blensor.kinect.scan_advanced(scanner_3, evd_file=directory+'_CAMERA3.pcd')
        blensor.kinect.scan_advanced(scanner_3, evd_file=directory+'_CAMERA3.pgm')

        # remove the object imported and the pointcloud data from the scene
        for ob in bpy.context.scene.objects:
            ob.select = ob.name.startswith("optimized_tsdf")
            bpy.ops.object.delete()
            ob.select = ob.name.startswith("Noisy")
            bpy.ops.object.delete()

##########
## main ##
##########
if __name__ == '__main__':
    # Initialize the socket
    c = comm_engine()
    
    # First we import the mesh files
    # Comment these lines out if you dont want to do that
    mesh_info = c.transmitter('init')

    # Start import procedure
    mesh_io(mesh_info)

    # Loop forever
    while True:
        # # Should we change the pose
        # mode = c.transmitter('mode')
        
        # if mode == 'mesh':
        #     mesh_info = c.transmitter('mesh_info')
        #     mesh_io(mesh_info)
        # else:
        # FIXME till I figure out a way to do this cleaner
        if True:
            # Get pose info
            pose_info = c.transmitter('pose')
            POSE_NO += 1
            
            # update robot pose
            update_pos(pose_info)

            # run scanner over YCB dataset
            scanner()

